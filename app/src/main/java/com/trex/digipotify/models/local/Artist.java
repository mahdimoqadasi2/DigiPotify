package com.trex.digipotify.models.local;

import java.io.Serializable;

public class Artist implements Serializable {
    private String name;
    private String img;
    private String popularity;

    private String followers;

    public Artist(String name, String img, String popularity, String followers) {
        this.name = name;
        this.img = img;
        this.popularity = popularity;
        this.followers = followers;
    }

    public String getName() {
        return name;
    }

    public String getImgUrl() {
        return img;
    }

    public String getPopularity() {
        return popularity;
    }

    public String getFollowers() {
        return followers;
    }
}
