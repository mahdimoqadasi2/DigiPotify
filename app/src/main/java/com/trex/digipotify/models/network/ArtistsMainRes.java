package com.trex.digipotify.models.network;

import com.google.gson.annotations.SerializedName;
import com.trex.digipotify.models.local.Artist;

import java.util.ArrayList;
import java.util.List;


public class ArtistsMainRes {

    @SerializedName("artists")
    private ArtistsData artistsData;

    public int artistsCount() {
        if (artistsData == null || artistsData.artistItems == null) return 0;
        return artistsData.artistItems.size();
    }

    public List<Artist> getMappedArtists() {
        List<Artist> list = new ArrayList<>();
        if (artistsData == null || artistsData.artistItems == null) return list;
        for (ArtistItem item : artistsData.artistItems) {
            list.add(new Artist(item.getName(), item.getFirstImage(), item.getPopularity(), item.getFollowers()));
        }
        return list;
    }

    public String getNextPageUrl() {
        return artistsData.nextItemsUrl;
    }

    public boolean hasMoreReults() {
        return artistsData.totalItemsCount > artistsData.offsetFromFirst + artistsData.artistItems.size();
    }

    private class ArtistsData {

        @SerializedName("next")
        private String nextItemsUrl;

        @SerializedName("total")
        private int totalItemsCount;

        @SerializedName("offset")
        private int offsetFromFirst;

        @SerializedName("previous")
        private String previousItemsUrl;

        @SerializedName("limit")
        private int maxCount;

        @SerializedName("href")
        private String thisReqUrl;

        @SerializedName("items")
        private List<ArtistItem> artistItems;
    }

    private class ArtistItem {

        @SerializedName("images")
        private List<ArtistImageItem> imgs;

        @SerializedName("followers")
        private FollowersData followersData;

        @SerializedName("genres")
        private List<String> genresData;

        @SerializedName("popularity")
        private int popularity;

        @SerializedName("name")
        private String name;

        @SerializedName("href")
        private String href;

        @SerializedName("id")
        private String uniqueId;

        @SerializedName("type")
        private String type; //is artist

        @SerializedName("external_urls")
        private ExternalUrls externalUrls;

        @SerializedName("uri")
        private String uri; //spotify uri

        private String getFirstImage() {
            return (imgs == null || imgs.size() < 1) ? "" : imgs.get(0).url;
        }

        String getName() {
            return (name == null) ? "" : name;
        }

        String getPopularity() {
            return String.valueOf(popularity);
        }

        String getFollowers() {
            return (followersData == null) ? "0" : String.valueOf(followersData.total);
        }
    }

    private class ArtistImageItem {

        @SerializedName("width")
        private int width;

        @SerializedName("height")
        private int height;

        @SerializedName("url")
        private String url;
    }

    private class ExternalUrls {
        @SerializedName("spotify")
        private String spotify;
    }

    private class FollowersData {
        @SerializedName("total")
        private int total;

        @SerializedName("href")
        private String href;
    }
}