package com.trex.digipotify.adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.trex.digipotify.R;
import com.trex.digipotify.models.local.Artist;
import com.trex.digipotify.network.RequestManager;
import com.trex.digipotify.pages.ArtistDetailsActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ArtistsResultAdapter extends RecyclerView.Adapter<ArtistsResultAdapter.ArtistsVH> {
    private List<Artist> mArtists = new ArrayList<>();

    @NotNull
    @Override
    public ArtistsVH onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.artist_row, parent, false);
        return new ArtistsVH(row);
    }

    @Override
    public void onBindViewHolder(@NotNull ArtistsVH holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return mArtists.size();
    }

    public void addArtists(List<Artist> newArtists, boolean isNewList) {
        if (isNewList) mArtists.clear();
        mArtists.addAll(newArtists);
        notifyDataSetChanged();
    }

    class ArtistsVH extends RecyclerView.ViewHolder {
        @OnClick(R.id.ar_root)
        void showDetail(View v) {
            Intent intent = new Intent(v.getContext(), ArtistDetailsActivity.class);
            intent.putExtra(ArtistDetailsActivity.ARTIST_KEY, mArtists.get(getAdapterPosition()));
            v.getContext().startActivity(intent);
        }

        @BindView(R.id.ar_img)
        ImageView img;
        @BindView(R.id.ar_txt)
        TextView name;

        ArtistsVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind() {
            Artist artist = mArtists.get(getAdapterPosition());
            name.setText(artist.getName());
            img.setImageResource(R.drawable.ic_loading);
            RequestManager.getInstance().loadImage(itemView.getContext(), artist.getImgUrl(), img);
        }
    }
}
