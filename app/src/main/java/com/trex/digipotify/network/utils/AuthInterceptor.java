package com.trex.digipotify.network.utils;

import android.content.Context;

import com.trex.digipotify.utils.SharedPrefs;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {

    private SharedPrefs mSharedPrefs;

    public AuthInterceptor(Context ctx) {
        this.mSharedPrefs = new SharedPrefs(ctx);
    }

    private Request setAccessToken(Request request) {
        return request.newBuilder()
                .header("Authorization", "Bearer " + mSharedPrefs.getAccessToken())
                .build();
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        return chain.proceed(setAccessToken(chain.request()));
    }
}