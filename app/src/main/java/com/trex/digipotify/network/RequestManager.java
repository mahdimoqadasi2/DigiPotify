package com.trex.digipotify.network;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.trex.digipotify.R;
import com.trex.digipotify.models.network.ArtistsMainRes;
import com.trex.digipotify.network.utils.AuthInterceptor;
import com.trex.digipotify.network.utils.SessionInterceptor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RequestManager {

    private static RequestManager instance;

    private static OkHttpClient mAuthorizedClient;
    private static AuthInterceptor mAuthInterceptor;
    private static SessionInterceptor mSessionInterceptor;

    public static final int SIGN_IN_REQ_CODE = 50;

    private static final String REDIRECT_URI = "http://mydigipay.com/";
    private static final String SEARCH_ENDPOINT = "https://api.spotify.com/v1/search";

    public static RequestManager getInstance() {
        if (instance == null) instance = new RequestManager();
        return instance;
    }

    private RequestManager() {
    }

    public synchronized Single<ArtistsMainRes> getArtistsByName(Context ctx, String artistName) {
        return Single.fromCallable(() -> {
            Map<String, String> map = new HashMap<>();
            map.put("q", artistName);
            map.put("type", "artist");
            return RequestManager.this.sendGet(ctx, SEARCH_ENDPOINT, map);
        }).map(response -> new Gson().fromJson(response.body().string(), ArtistsMainRes.class))
                .subscribeOn(Schedulers.io());
    }

    public synchronized void loadImage(Context ctx, String url, ImageView view) {
        view.setTag(url);
        Disposable ignore = Single.fromCallable(() -> BitmapFactory.decodeStream(RequestManager.this.sendGet(ctx, url, null).body().byteStream()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(bitmap -> {
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    scaled.compress(Bitmap.CompressFormat.JPEG, 60, os);
                    return BitmapFactory.decodeStream(new ByteArrayInputStream(os.toByteArray()));
                }).subscribe(result -> {
                    if (view.getTag().equals(url))
                        view.setImageBitmap(result);
                }, throwable -> view.setImageResource(R.drawable.unknown_singer));
    }

    private synchronized Response sendGet(Context ctx, String url, Map<String, String> params) {
        try {
            HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder();
            if (params != null) {
                for (Map.Entry<String, String> param : params.entrySet()) {
                    httpBuider.addQueryParameter(param.getKey(), param.getValue());
                }
            }
            Request request = new Request.Builder().url(httpBuider.build()).build();
            return getAuthorizedClient(ctx).newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized void reqLogin(Activity act) {
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(act.getString(R.string.spotify_client_id), AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
        builder.setScopes(new String[]{"streaming"});
        AuthenticationRequest request = builder.build();
        AuthenticationClient.openLoginActivity(act, SIGN_IN_REQ_CODE, request);
    }

    private synchronized OkHttpClient getAuthorizedClient(Context ctx) {
        if (mAuthorizedClient == null)
            mAuthorizedClient = new OkHttpClient.Builder()
                    .cache(new Cache(ctx.getCacheDir(), 20 * 1024 * 1024)) // 20MB
                    .addInterceptor(getAuthInterceptor(ctx))
                    .addInterceptor(getSessionInterceptor(ctx))
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
        return mAuthorizedClient;
    }

    private synchronized AuthInterceptor getAuthInterceptor(Context ctx) {
        if (mAuthInterceptor == null)
            mAuthInterceptor = new AuthInterceptor(ctx);
        return mAuthInterceptor;
    }

    private synchronized SessionInterceptor getSessionInterceptor(Context ctx) {
        if (mSessionInterceptor == null)
            mSessionInterceptor = new SessionInterceptor(ctx);
        return mSessionInterceptor;
    }

    public synchronized Single<ArtistsMainRes> loadNextPage(Context ctx, String nextPageUrl) {
        return Single.fromCallable(() -> RequestManager.this.sendGet(ctx, nextPageUrl, null)).map(response -> new Gson().fromJson(response.body().string(), ArtistsMainRes.class))
                .subscribeOn(Schedulers.io());
    }
}
