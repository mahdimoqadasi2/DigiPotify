package com.trex.digipotify.network.utils;

import android.content.Context;

import androidx.annotation.NonNull;

import com.trex.digipotify.utils.RouteHelper;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class SessionInterceptor implements Interceptor {

    private Context ctx;

    public SessionInterceptor(Context ctx) {
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        switch (response.code()) {
            case HTTP_UNAUTHORIZED:
                RouteHelper.invalidate(ctx);
                throw new IOException();
            case HTTP_FORBIDDEN:
                RouteHelper.invalidate(ctx);
                throw new IOException();
        }
        return response;
    }
}