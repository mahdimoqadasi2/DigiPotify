package com.trex.digipotify.pages;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.trex.digipotify.R;
import com.trex.digipotify.models.local.Artist;
import com.trex.digipotify.network.RequestManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArtistDetailsActivity extends AppCompatActivity {

    public static final String ARTIST_KEY = "artist_key";

    @BindView(R.id.ad_img)
    ImageView img;
    @BindView(R.id.ad_txtName)
    TextView txtName;
    @BindView(R.id.ad_txtPopularity)
    TextView txtPopularity;
    @BindView(R.id.ad_txtFollowersCount)
    TextView txtFollowers;
    private Artist mCurArtist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_details);
        ButterKnife.bind(this);
        setValues();
    }

    private void setValues() {
        if (getIntent() == null) return;
        mCurArtist = (Artist) getIntent().getSerializableExtra(ARTIST_KEY);
        img.setImageResource(R.drawable.ic_loading);
        RequestManager.getInstance().loadImage(this, mCurArtist.getImgUrl(), img);
        txtName.setText(mCurArtist.getName());
        txtPopularity.setText(mCurArtist.getPopularity());
        txtFollowers.setText(mCurArtist.getFollowers());
    }
}
