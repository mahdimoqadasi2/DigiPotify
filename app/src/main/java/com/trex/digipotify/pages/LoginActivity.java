package com.trex.digipotify.pages;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.trex.digipotify.pages.MainActivity;
import com.trex.digipotify.R;
import com.trex.digipotify.network.RequestManager;
import com.trex.digipotify.utils.SharedPrefs;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    RequestManager mReqManager;
    SharedPrefs mSharedPrefs;

    @OnClick(R.id.al_btnSubmit)
    public void submit(View v) {
        mReqManager.reqLogin(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mReqManager = RequestManager.getInstance();
        mSharedPrefs = new SharedPrefs(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestManager.SIGN_IN_REQ_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);
            switch (response.getType()) {
                case TOKEN:
                    mSharedPrefs.setAccsToken(response.getAccessToken());
                    finish();
                    startActivity(new Intent(this, MainActivity.class));
                    break;
                case ERROR:
                    Toast.makeText(this, response.getError() + "\nPlease try again", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(this, "Login cancelled. Please try again", Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    }
}
