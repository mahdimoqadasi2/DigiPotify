package com.trex.digipotify.pages;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.trex.digipotify.R;
import com.trex.digipotify.adapters.ArtistsResultAdapter;
import com.trex.digipotify.models.local.Artist;
import com.trex.digipotify.models.network.ArtistsMainRes;
import com.trex.digipotify.network.RequestManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    RequestManager mReqManager;
    CompositeDisposable mDisposeBag;
    ArtistsResultAdapter mAdp;

    @BindView(R.id.am_rclArtists)
    RecyclerView rclArtists;
    @BindView(R.id.am_txtEmpty)
    TextView txtEmpty;
    @BindView(R.id.am_prgLoading)
    ProgressBar prgLoading;

    private boolean isLoading;

    private String lastQuery = null;
    private ArtistsMainRes lastResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Search Page");
        def();
    }

    private void def() {
        ButterKnife.bind(this);
        mReqManager = RequestManager.getInstance();
        mDisposeBag = new CompositeDisposable();

        mAdp = new ArtistsResultAdapter();
        rclArtists.setAdapter(mAdp);
        rclArtists.setLayoutManager(new GridLayoutManager(this, 2));

        rclArtists.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1) && lastQuery != null)
                    loadNextPage(lastQuery);
            }
        });
    }

    private void loadNextPage(String query) {
        if (!query.equals(lastQuery)) {// it's first page
            setLoading(true);
            lastQuery = query;
            Disposable d = mReqManager.getArtistsByName(MainActivity.this, query)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> {
                        lastResponse = response;
                        Toast.makeText(MainActivity.this, response.artistsCount() + " items received", Toast.LENGTH_SHORT).show();
                        updateRcl(response.getMappedArtists(), true);
                        setLoading(false);
                    }, t -> {
                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        setLoading(false);
                    });
            mDisposeBag.add(d);
        } else if (lastResponse.hasMoreReults()) { //should get next page
            setLoading(true);
            Disposable d = mReqManager.loadNextPage(MainActivity.this, lastResponse.getNextPageUrl())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> {
                        lastResponse = response;
                        Toast.makeText(MainActivity.this, response.artistsCount() + " new items added", Toast.LENGTH_SHORT).show();
                        rclArtists.smoothScrollBy(0, 150);
                        updateRcl(response.getMappedArtists(), false);
                        setLoading(false);
                    }, t -> {
                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        setLoading(false);
                    });
            mDisposeBag.add(d);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!isLoading)
                    loadNextPage(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        return true;
    }

    private void updateRcl(List<Artist> newArtists, boolean isNewList) {
        mAdp.addArtists(newArtists, isNewList);
        if (mAdp.getItemCount() > 0)
            txtEmpty.setVisibility(View.GONE);
        else
            txtEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        mDisposeBag.dispose();
        super.onDestroy();
    }

    public void setLoading(boolean shouldLoad) {
        isLoading = shouldLoad;
        if (shouldLoad)
            prgLoading.setVisibility(View.VISIBLE);
        else
            prgLoading.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {

        super.onPause();
    }
}