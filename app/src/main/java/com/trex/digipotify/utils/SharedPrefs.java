package com.trex.digipotify.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {

    private static final String MAIN_INFO = "main_info";

    private static final String KEY_ACCESS_TOKEN = "key_access_token";

    private SharedPreferences mSP;

    public SharedPrefs(Context ctx) {
        mSP = ctx.getSharedPreferences(MAIN_INFO, Context.MODE_PRIVATE);
    }

    public Boolean isLoggedIn() {
        return !mSP.getString(KEY_ACCESS_TOKEN, "").equals("");
    }

    public String getAccessToken() {
        return mSP.getString(KEY_ACCESS_TOKEN, "");
    }

    public void setAccsToken(String token) {
        mSP.edit().putString(KEY_ACCESS_TOKEN, token).apply();
    }

    void clearData() {
        mSP.edit().clear().apply();
    }
}
