package com.trex.digipotify.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.trex.digipotify.pages.LoginActivity;

public class RouteHelper {

    public static void openBrowserIntent(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static void invalidate(Context ctx) {
        new SharedPrefs(ctx).clearData();
        Intent intent = new Intent(ctx, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ctx.startActivity(intent);

    }
}
